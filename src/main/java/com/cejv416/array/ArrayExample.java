package com.cejv416.array;

import com.cejv416.bean.PeopleBean;
import java.util.Scanner;

/**
 * Example using an Array
 *
 * @author Ken Fogel
 */
public class ArrayExample {

    private final Scanner sc;
    private final PeopleBean[] people;

    /**
     * Constructor instantiates the Scanner and the Array
     * @param numberOfPeople
     */
    public ArrayExample(int numberOfPeople) {
        sc = new Scanner(System.in);
        people = new PeopleBean[numberOfPeople]; 
    }

    /**
     * Ask for user input as a String
     *
     * @param prompt
     * @return the user input
     */
    private String getUserInfo(String prompt) {
        System.out.println(prompt);
        String info = sc.next();        // read the first string on the line
        sc.nextLine();               // discard any other data entered on the line
        return info;
    }

    /**
     * Add People to the array. You can only add as many people as the length of
     * the array allows
     */
    private void loadPeople() {
        String theName;
        String theCity;
        PeopleBean person;
        System.out.println("Enter " + people.length + " People");
        for (int x = 0; x < people.length; ++x) {
            theName = getUserInfo("Enter name # " + (x+1) + ":");
            theCity = getUserInfo("Enter city # " + (x+1) + ":");
            person = new PeopleBean(theName, theCity);
            people[x] = person;
        }
    }

    /**
     * Display the people
     */
    private void displayPeople() {
        for (PeopleBean person : people) {
            System.out.println("Name " + person.getName());
            System.out.println("City " + person.getCity());
        }
    }

    /**
     * Program controller
     */
    public void perform() {
        loadPeople();
        displayPeople();
    }

    /**
     * Where it all begins
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        new ArrayExample(4).perform();
    }

}
