package com.cejv416.arrayList;

import com.cejv416.bean.PeopleBean;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

/**
 * Example using an ArrayList
 *
 * @author Ken Fogel
 */
public class ArrayListExample {

    private final Scanner sc;
    private final ArrayList<PeopleBean> people;

    /**
     * Default Constructor Instantiate the Scanner and the ArrayList
     */
    public ArrayListExample() {
        sc = new Scanner(System.in);
        people = new ArrayList<>();
    }

    /**
     * Ask for user input as a String
     *
     * @param prompt
     * @return the user input
     */
    private String getUserInfo(String prompt) {
        System.out.println(prompt);
        String info = sc.next();        // read the first string on the line
        sc.nextLine();               // discard any other data entered on the line
        return info;
    }

    /**
     * Add People beans to the ArrayList. There is no limit other than available
     * memory for the size of an ArrayList
     */
    private void loadPeople() {
        int x = 1;
        String theName;
        String theCity;
        PeopleBean person;
        String choice;
        System.out.println("Enter People");
        do {
            theName = getUserInfo("Enter name # " + x + ":");
            theCity = getUserInfo("Enter city # " + x + ":");
            person = new PeopleBean(theName, theCity);
            people.add(person);
            ++x;

            // see if the user wants to continue
            choice = getUserInfo("Continue? (y/n): ");
            System.out.println();
        } while (!choice.equalsIgnoreCase("n"));
    }

    /**
     * Display the people
     */
    private void displayPeople() {
        System.out.println("Unsorted");
        for (PeopleBean person : people) {
            System.out.println("Name " + person.getName());
            System.out.println("City " + person.getCity());
        }
        System.out.println("\nSorted");
        Collections.sort(people);
        for (PeopleBean person : people) {
            System.out.println("Name " + person.getName());
            System.out.println("City " + person.getCity());
        }
    }

    /**
     * Program controller
     */
    public void perform() {
        loadPeople();
        displayPeople();
    }

    /**
     * Where it all begins
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        new ArrayListExample().perform();
    }

}
