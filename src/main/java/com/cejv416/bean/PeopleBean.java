package com.cejv416.bean;

/**
 * Bean to hold People data
 *
 * @author Ken Fogel
 */
public class PeopleBean implements Comparable<PeopleBean>{

    private String name;
    private String city;

    public PeopleBean(String name, String city) {
        this.name = name;
        this.city = city;
    }

    public PeopleBean() {
        name = "";
        city = "";
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Override
    public String toString() {
        return "StuffBean{" + "name=" + name + ", city=" + city + '}';
    }

    @Override
    public int compareTo(PeopleBean pb) {
        return name.compareTo(pb.name);
    }

}
